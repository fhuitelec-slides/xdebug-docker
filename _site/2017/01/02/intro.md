## Why using xdebug?

<!-- .element: class="fragment" --> - debugging

<!-- .element: class="fragment" --> - code exploration

Note:
- c'est pour rappel
- je ne vous apprends rien

--

## Some intro

<!-- .element: class="fragment" --> - Lots of resource 📚

<!-- .element: class="fragment" --> - Sometimes messy 💥

Note:
- D'où l'intérêt de cette présentation

--

### A bit of elegance

<!-- .element: class="fragment" --> - Not reinvent the wheel - again

<!-- .element: class="fragment" --> - Simplicity

<!-- .element: class="fragment" --> - The right abstraction

<!-- .element: class="fragment" --> - Best practices

Note:
- la bonne abstraction **en termes d'infrastructure**