## Demo 🎉

_Murphy's law ain't gonna have this demo_

<img data-src="resources/img/murphys_law.gif" style="border:0;">

--

### Quick look into the code

--

### Bit of context

Note:
- bootstrappé l'application
- jolis tests
- **distribuer** au sein de l'équipe => **packager** avec Docker

--

### Quick look into the infra

--

### Makefile

_to rule 'em all_

Note:
- abstraire **complexité**
- faciliter cette **prez**

--

### Refacto - step 1

Note:
- `Dockerfile`
- `grep xdebug`
- `grep remote_host`

--

### Some `xdebug` explanation

Note:
- **extension** | greffe au **moteur PHP**
- **trigger session** de debug/profilage
- se connecte **1er client** qui **accepte** la connexion
	- => `le remote`

--

### Some `remote_host` explanation

<img data-src="resources/img/xdebug-docker.png" style="border:0;">

Note:
- pas de grosse notions de networking
- encore moins appliqué à docker
- 2 choses que je sais :
	- veux pas du localhost
		- => pointe **loopback** du container
	- tout de même **directement accéder** au `host`

--

### Simple way

```
# Adresse IP de classe A arbitraire
# Attention aux collisions
sudo ifconfig lo0 alias 10.0.2.2 
```

--

### Refacto - step 2

Note:
- `docker-compose.yml`
- **interagir** conf xdebug au **runtime**

--

### Optimisation

Note:
- substitution de variable