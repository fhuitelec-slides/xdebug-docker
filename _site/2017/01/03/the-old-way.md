<img data-src="resources/img/old.gif" style="border:0;">
## The old way

--

### `var_dump()`

_and you better `die`_

--

The code

```php
/** @param $bar string */
function pissingIntoTheWind($bar) {
	$foo = new \stdClass();
	$foo->bar = $bar;
	$baz = [
		'foo' => $foo
	];

	// Todo: remove yolo debug
	var_dump($baz);
	die();

	return $baz;
}
```

--

The output

```
pissingIntoTheWind('💔');
```

```php
/The/FilePath/test.php:14:
array(1) {
  'foo' =>
  class stdClass#1 (1) {
    public $bar =>
    string(4) "💔"
  }
}
```

--

### `dump()`

_coz we love you so much, Symfony_

--

```bash
compose require --dev symfony/var-dumper
```

```php
array:1 [
  "foo" => {#4
    +"bar": "💔"
  }
]
```