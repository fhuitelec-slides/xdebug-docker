## Quick demo on Evaneos

<img data-src="resources/img/aint_time_for_this.gif" style="border:0;">

--

### Extension xdebug

Documentation in [here]() and Chrome extension in [here]() `todo`

Note:
- lancer une **session** xdebug automatiquement
- chaque **requête**
- utilise **query parameters** & cookies

--

### Modification `docker/dev`::`php`

```yaml
    environment:
        XDEBUG_CONFIG: "remote_host=10.0.2.2"
        PHP_IDE_CONFIG: "serverName=www"
```

Note:
- un peu sale
- pour l'exemple