# Slides template

## Description

Slides for a presentation at Evaneos for the developers community about Docker and debugging PHP with xdebug

## Usage

### Fork

- Fork me
- :warning: In **Settings**/**General**, remove the fork relationship! :warning:
- Change my **repo name**, **description** and **logo**
- Activate a runner to enable CI Pipelines

### Dependencies

- [Install Jekyll](https://jekyllrb.com/docs/installation/)
- Pull `reveal.js` submodule with `git submodule update --init`

### Getting started

- Edit `_config.yml` (especially title) to customize your slides
- Create your slides in `_posts`
- `jekyll serve` to watch them in real time

### Passord protecting

Once you're done, if you need to password-protect your pages, go [here](https://fhuitelec.gitlab.io/html-encryptor/) and follow the instructions.

Be sure to add a **page title** and **instructions** before generating the new HTML page.

Download the HTML file with password prompt and paste it in `_site/index.html`

To try the password prompt:

```
jekyll serve --skip-initial-build
```

Commit and push and you're done!
